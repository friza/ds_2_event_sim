package core;

import core.model.Type;

/**
 * Created by Tomino on 02/03/16.
 */
public interface IForm {

    void redrawLabels(final Type simulationType, final double actualReplicationTime);
    void redrawGraphs();

    void simulationStarted();
    void simulationFinished();
    void simulationPausedOrResume(final boolean isPaused, final double actualReplicationTime);
}
