package core;

import core.model.Speed;
import core.model.Type;
import generator.SeedGenerator;
import model.GeneratorModule;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.BaseEvent;
import model.event.simulation.CustomerArrivalEvent;
import model.event.simulation.EndOfDayEvent;
import model.event.sysEvent.SlowDownEvent;
import model.object.Car;
import model.object.Employee;
import model.object.ReplicationProcessFlow;
import model.object.enums.EmployeeType;

import java.util.ArrayList;

/**
 * Created by Tomino on 11/03/16.
 */
public class Simulation extends CoreSimulation {

    private static final long SIM_START_TIME = 0;
    private static final double LENGTH_OF_DAY = 8 * 60 * 60;

    private int mCardId;

    private final int mCountOfVendors;
    private final int mCountOfMechanics;
    private final double mMaxReplicationTime; // 90 days, 1 day has only 8 hours length

    private final GeneratorModule mGeneratorModule;
    private final ReplicationProcessFlow mReplicationProcessFlow;

    private final ArrayList<Employee> mVendorsArray;
    private final ArrayList<Employee> mMechanicsArray;

    /**
     * WHOLE SIMULATION WILL BE IN SECONDS !!! NOT IN HOURS !!!
     * @param iForm who cares
     * @param countOfReplication who cares
     * @param maxReplicationTime in seconds > 90 days, since 1 day will have only 8 hours
     * @param countOfVendors who cares
     * @param countOfMechanics who cares
     */
    public Simulation(IForm iForm, final long graphPeriod, final long countOfReplication, final double maxReplicationTime,
                      final int countOfVendors, final int countOfMechanics) {
        super(iForm, graphPeriod, countOfReplication);

        mCountOfVendors = countOfVendors;
        mCountOfMechanics = countOfMechanics;
        mMaxReplicationTime = maxReplicationTime;

        mReplicationProcessFlow = new ReplicationProcessFlow(mMaxReplicationTime);

        mVendorsArray = new ArrayList<>();
        for (int i = 0; i < mCountOfVendors; i++) {
            mVendorsArray.add(new Employee(EmployeeType.VENDOR, i));
        }

        mMechanicsArray = new ArrayList<>();
        for (int i = 0; i < mCountOfMechanics; i++) {
            mMechanicsArray.add(new Employee(EmployeeType.MECHANIC, i));
        }

        mGeneratorModule = new GeneratorModule(new SeedGenerator());
    }

    @Override
    protected void doReplication(int numberOfReplication) {
        while (/*mActualReplicationTime < mMaxReplicationTime && */mEventFront.size() > 0) {
            BaseEvent actualEvent = getOncomingEvent();

            mActualReplicationTime = actualEvent.getEventTime();
            mReplicationProcessFlow.checkSwitch(mActualReplicationTime);
            ReplicationResult.mReplicationTime = mActualReplicationTime;

            if (mActualReplicationTime >= mMaxReplicationTime) {
                    if (mEventFront.size() == 0 && actualEvent instanceof SlowDownEvent) {
                        break;
                    }
            }

            //if (mActualReplicationTime < mMaxReplicationTime) {
                actualEvent.execute(this);
            //}

            if (mSimulationMode == Speed.SLOW) {
                drawReplicationResults();
            }

            while (mSimulationIsPaused) {
                try {
                    Thread.sleep(SLEEP_FOR_ONE_SECOND);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (mSimulationIsStopped) {
                    mForm.simulationFinished();
                    return;
                }
            }

            if (mSimulationIsStopped) {
                mForm.simulationFinished();
                return;
            }
        }

        saveResults(numberOfReplication);
    }

    @Override
    protected void reset() {
        ReplicationResult.reset();
        clearEventFront();

        mReplicationProcessFlow.reset();

        mActualReplicationTime = 0;
        mEventId = 0;
        mCardId = 0;

        for (Employee employee : mVendorsArray) {
            employee.setOccupation(false);
        }
        ReplicationResult.mFreeVendors.addAll(mVendorsArray);

        for (Employee employee : mMechanicsArray) {
            employee.setOccupation(false);
        }
        ReplicationResult.mFreeMechanics.addAll(mMechanicsArray);

        mGeneratorModule.reset();
        planInitEvents();
    }

    @Override
    public double getMaxReplicationTime() {
        return mMaxReplicationTime;
    }

    @Override
    protected void drawReplicationResults() {
        mForm.redrawLabels(Type.REPLICATION, mActualReplicationTime);
    }

    @Override
    protected void saveResults(final long numberOfReplication) {
        SimulationResult.mCountOfReplication = numberOfReplication;
        SimulationResult.mAverageCustomerTimeInService += ReplicationResult.getAverageCustomerTimeInService();
        SimulationResult.mAverageCustomerTimeInServiceSquare += Math.pow(ReplicationResult.getAverageCustomerTimeInService(), 2);
        SimulationResult.mAverageCountOfFiredCustomers += ReplicationResult.mCountOfFiredCustomers / 90;
        SimulationResult.mAverageCountOfFiredCustomersSquare += Math.pow(ReplicationResult.mCountOfFiredCustomers / 90, 2);

        SimulationResult.mAverageAcceptanceWaitTime += ReplicationResult.mAverageAcceptanceWaitTime / ReplicationResult.mAcceptanceWaitTimeIncremented;
        SimulationResult.mAverageAcceptanceWaitTimeSquare += Math.pow(ReplicationResult.mAverageAcceptanceWaitTime / ReplicationResult.mAcceptanceWaitTimeIncremented, 2);
        SimulationResult.mAverageForRepairWaitTime += ReplicationResult.mAverageForRepairWaitTime / ReplicationResult.mForRepairWaitTimeIncremented;
        SimulationResult.mAverageForRepairWaitTimeSquare += Math.pow(ReplicationResult.mAverageForRepairWaitTime / ReplicationResult.mForRepairWaitTimeIncremented, 2);
        SimulationResult.mAverageForHandoverWaitTime += ReplicationResult.mAverageForHandoverWaitTime / ReplicationResult.mForHandoverWaitTimeIncremented;
        SimulationResult.mAverageForHandoverWaitTimeSquare += Math.pow(ReplicationResult.mAverageForHandoverWaitTime / ReplicationResult.mForHandoverWaitTimeIncremented, 2);

        SimulationResult.mAverageCustomerWaitingForCar += ReplicationResult.getAverageCustomerWaitingForCar();
        SimulationResult.mAverageCustomerWaitingForCarSquare += Math.pow(ReplicationResult.getAverageCustomerWaitingForCar(), 2);
        SimulationResult.mAverageLengthOfRepair += ReplicationResult.getAverageRepairLength();
        SimulationResult.mAverageLengthOfRepairSquare += Math.pow(ReplicationResult.getAverageRepairLength(), 2);
        SimulationResult.mAverageCoolingUpLength += mReplicationProcessFlow.getCoolingUpLength();
        SimulationResult.mAverageCoolingUpLengthSquare += Math.pow(mReplicationProcessFlow.getCoolingUpLength(), 2);

        SimulationResult.mAverageLengthOfAcceptanceQueue += ReplicationResult.mAverageLengthOfAcceptanceQueue / SimulationResult.mReplicationLength;
        SimulationResult.mAverageLengthOfAcceptanceQueueSquare += Math.pow(ReplicationResult.mAverageLengthOfAcceptanceQueue / SimulationResult.mReplicationLength, 2);
        SimulationResult.mAverageLengthOfForRepairQueue += ReplicationResult.mAverageLengthOfForRepairQueue / SimulationResult.mReplicationLength;
        SimulationResult.mAverageLengthOfForRepairQueueSquare += Math.pow(ReplicationResult.mAverageLengthOfForRepairQueue / SimulationResult.mReplicationLength, 2);
        SimulationResult.mAverageLengthOfForHandoverQueue += ReplicationResult.mAverageLengthOfForHandoverQueue / SimulationResult.mReplicationLength;
        SimulationResult.mAverageLengthOfForHandoverQueueSquare += Math.pow(ReplicationResult.mAverageLengthOfForHandoverQueue / SimulationResult.mReplicationLength, 2);

        SimulationResult.mAverageLengthOfFreeVendorQueue += ReplicationResult.mAverageLengthOfFreeVendorQueue / SimulationResult.mReplicationLength;
        SimulationResult.mAverageLengthOfFreeVendorQueueSquare += Math.pow(ReplicationResult.mAverageLengthOfFreeVendorQueue / SimulationResult.mReplicationLength, 2);
        SimulationResult.mAverageLengthOfFreeMechanicsQueue += ReplicationResult.mAverageLengthOfFreeMechanicsQueue / SimulationResult.mReplicationLength;
        SimulationResult.mAverageLengthOfFreeMechanicsQueueSquare += Math.pow(ReplicationResult.mAverageLengthOfFreeMechanicsQueue / SimulationResult.mReplicationLength, 2);
    }

    public GeneratorModule provideGenerators() {
        return mGeneratorModule;
    }

    @Override
    public void changeMode(final Speed simulationMode) {
        mSimulationMode = simulationMode;

        if (mSimulationMode == Speed.SLOW) {
            planEvent(new SlowDownEvent(getEventId(), mActualReplicationTime));
        }
    }

    private void planInitEvents() {
        final Car car = generateCar();
        planEvent(generateEndOfDayEvent());
        planEvent(new CustomerArrivalEvent(getEventId(), car.getArrivalTime(), car));

        if (getSimulationMode() == Speed.SLOW) {
            planEvent(new SlowDownEvent(getEventId(), SIM_START_TIME));
        }
    }

    private int getCarId() {
        mCardId++;
        return mCardId;
    }

    public Car generateCar() {
        return new Car(getCarId(), getActualReplicationTime() + provideGenerators().getNextArrival(), provideGenerators());
    }

    public EndOfDayEvent generateEndOfDayEvent() {
        return new EndOfDayEvent(getEventId(), getActualReplicationTime() + LENGTH_OF_DAY, null);
    }

    public ReplicationProcessFlow getReplicationProcessFlow() {
        return mReplicationProcessFlow;
    }
}
