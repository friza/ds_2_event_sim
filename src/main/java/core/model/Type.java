package core.model;

/**
 * Created by Tomino on 11/03/16.
 */
public enum Type {
    REPLICATION, GLOBAL
}
