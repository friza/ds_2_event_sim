package core;

import core.model.Speed;
import core.model.Type;
import generator.SeedGenerator;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.BaseEvent;
import model.event.sysEvent.StartSimulationEvent;

import java.util.PriorityQueue;

/**
 * Created by Tomino on 11/03/16.
 */
public abstract class CoreSimulation extends Thread {

    protected final static int GRAPH_MOD = 10000;
    protected final static int SLEEP_FOR_ONE_SECOND = 1 * 1000;

    protected final IForm mForm;
    protected final SeedGenerator mSeedGenerator;

    protected Speed mSimulationMode = Speed.SLOW;

    private long mCountOfReplication;
    private long mGraphPeriod;
    protected double mActualReplicationTime;  // in hours

    protected boolean mSimulationIsStopped = false;
    protected boolean mSimulationIsPaused = false;

    protected long mEventId;
    protected PriorityQueue<BaseEvent> mEventFront;

    public CoreSimulation(final IForm iForm, final long graphPeriod, final long countOfReplication) {
        mForm = iForm;
        mCountOfReplication = countOfReplication;

        mGraphPeriod = graphPeriod;
        mEventId = 0;

        mEventFront = new PriorityQueue<>();
        mSeedGenerator = new SeedGenerator();
    }

    // --- SIMULATION MANAGEMENT

    public abstract void changeMode(final Speed simulationMode);

    public Speed getSimulationMode() {
        return mSimulationMode;
    }

    public void stopSimulation() {
        mSimulationIsStopped = true;
    }

    public void pauseSimulation() {
        mSimulationIsPaused = !mSimulationIsPaused;
        mForm.simulationPausedOrResume(mSimulationIsPaused, mActualReplicationTime);
    }

    // --- SIMULATION

    public long getEventId() {
        mEventId++;
        return mEventId;
    }

    public void planEvent(final BaseEvent event) {
        mEventFront.add(event);
    }

    public void clearEventFront() {
        mEventFront.clear();
    }

    protected BaseEvent getOncomingEvent() {
        return mEventFront.poll();
    }

    protected abstract void reset();

    protected abstract void doReplication(final int numberOfReplication);

    @Override
    public void run() {
        mForm.simulationStarted();
        planEvent(new StartSimulationEvent(getEventId(), 0));

        ReplicationResult.init();
        SimulationResult.init(getMaxReplicationTime());

        for (int i = 1; i <= mCountOfReplication; i++) {
            reset();
            doReplication(i);

            if (mSimulationMode == Speed.SLOW) {
                drawResults(i);
            }

            if (mSimulationIsStopped) {
                mForm.simulationFinished();
                return;
            }
        }

        mForm.simulationFinished();
    }

    protected void recalculateEvents(final double maxReplicationTime) {
        for (BaseEvent event : mEventFront) {
            event.recalculateEventTime(maxReplicationTime);
        }
    }

    public double getActualReplicationTime() {
        return mActualReplicationTime;
    }

    public abstract double getMaxReplicationTime();

    // --- RESULTS

    protected abstract void saveResults(final long numberOfReplication);
    protected abstract void drawReplicationResults();

    protected void drawResults(final int replicationNumber) {
        mForm.redrawLabels(Type.GLOBAL, mActualReplicationTime);
        if (replicationNumber % mGraphPeriod == 0) {
            mForm.redrawGraphs();
        }
    }
}