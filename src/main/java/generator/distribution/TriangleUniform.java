package generator.distribution;

import generator.BaseGenerator;

import java.util.Random;

/**
 * Created by Tomino on 22/03/2017.
 */
public class TriangleUniform extends BaseGenerator<Double> {

    private final double mMin;
    private final double mMax;
    private final double mModus;

    public TriangleUniform(final long seed, final double min, final double max, final double modus) {
        mMin = min;
        mMax = max;
        mModus = modus;

        mRandom = new Random(seed);
    }

    @Override
    public Double generate() {
        double result;
        double randomNumber = mRandom.nextDouble();

        if (randomNumber <= ((mModus - mMin) / (mMax - mMin))) {
            result = mMin + Math.sqrt(randomNumber * (mMax - mMin) * (mModus - mMin));
        } else {
            result = mMax - Math.sqrt((1 - randomNumber) * (mMax - mMin) * (mMax - mModus));
        }

        if (result < 0) {
            result = Math.abs(result);
        }

        return result;
    }
}
