package generator.distribution;

import generator.BaseGenerator;

import java.util.Random;

/**
 * Created by Tomino on 02/03/16.
 */
public class Exponential extends BaseGenerator<Double> {

    private final double mParam;

    public Exponential(final long seed, final double param) {
        mParam = param;
        mRandom = new Random(seed);
    }

    @Override
    public Double generate() {
        double result = -(Math.log(mRandom.nextDouble())) * mParam;

        if (result < 0) {
            result = Math.abs(result);
        }

        return result;
    }
}
