package generator.distribution;

import generator.BaseGenerator;

/**
 * Created by Tomino on 14/03/16.
 */
public class PoissonFlow extends BaseGenerator<Double> {

    private final Exponential mGenerator;

    public PoissonFlow(final long seed, final double count, final double modValue) {
        mGenerator = new Exponential(seed, getMedian(count, modValue));
    }

    private double getMedian(final double count, final double modValue) {
        return modValue / count;
    }

    @Override
    public Double generate() {
        return mGenerator.generate();
    }
}
