package generator.distribution;

import generator.BaseGenerator;
import generator.PercentGenerator;
import generator.SeedGenerator;
import generator.object.DiscreteUniformConstruct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomino on 02/03/16.
 */
public class DiscreteEmpirical extends BaseGenerator<Long> {

    private PercentGenerator mPercentGenerator;
    private ArrayList<DiscreteUniform> mDiscreetUniformArray;

    public DiscreteEmpirical(final SeedGenerator seedGenerator, final List<DiscreteUniformConstruct> discreteUniformConstructArray) {
        mPercentGenerator = new PercentGenerator(seedGenerator.generate());

        mDiscreetUniformArray = new ArrayList<>();
        for (DiscreteUniformConstruct construct : discreteUniformConstructArray) {
            mDiscreetUniformArray.add(new DiscreteUniform(seedGenerator.generate(),
                    construct.getMin(), construct.getMax(), construct.getProbability()));
        }
    }

    @Override
    public Long generate() {
        double percentValue = mPercentGenerator.generate();
        for (DiscreteUniform generator : mDiscreetUniformArray) {
            if (percentValue < generator.getProbability()) {
                return generator.generate();
            } else {
                percentValue -= generator.getProbability();
            }
        }

        return -1L;
    }
}
