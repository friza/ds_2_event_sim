package generator.object;

/**
 * Created by Tomino on 03/03/16.
 */
public class DiscreteUniformConstruct {

    private double mMin;
    private double mMax;
    private double mProbability;

    public DiscreteUniformConstruct(final double min, final double max, final double probability) {
        mMin = min;
        mMax = max;
        mProbability = probability;
    }

    public double getMin() {
        return mMin;
    }

    public double getMax() {
        return mMax;
    }

    public double getProbability() {
        return mProbability;
    }
}
