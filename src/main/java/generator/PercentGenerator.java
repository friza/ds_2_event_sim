package generator;

import java.util.Random;

/**
 * Created by Tomino on 02/03/16.
 */
public class PercentGenerator extends BaseGenerator<Double> {

    public PercentGenerator(final long seed) {
        mRandom = new Random(seed);
    }

    @Override
    public Double generate() {
        return mRandom.nextDouble();
    }
}
