package model;

import generator.PercentGenerator;
import generator.SeedGenerator;
import generator.distribution.*;
import generator.object.DiscreteUniformConstruct;

import java.util.Arrays;

/**
 * Created by Tomino on 22/03/2017.
 */
public class GeneratorModule {

    private final double ONE_HOUR_TO_SECONDS = 3600;
    private final double COUNT_OF_CARS_IN_HOUR = 12;

    private final SeedGenerator mSeedGenerator;

    private PoissonFlow mCustomerArrivalGenerator;
    private PercentGenerator mNumberOfRepairsGenerator;
    private PercentGenerator mTypeOfRepairGenerator;

    private DiscreteUniform mEasyRepairTimeGenerator;
    private DiscreteEmpirical mMediumRepairTimeGenerator;
    private DiscreteUniform mHardRepairTimeGenerator;

    private TriangleUniform mTravelTimeForRepairGenerator;
    private TriangleUniform mTravelTimeFromRepairGenerator;

    private ContinuousUniform mLengthOfOrderAcceptanceGenerator;
    private ContinuousUniform mLengthOfCarAcceptanceGenerator;
    private ContinuousUniform mLengthOfCarHandoverGenerator;

    /**
     * Whole module return times in SECONDS !!!
     * @param seedGenerator base simulation seed generator
     */
    public GeneratorModule(final SeedGenerator seedGenerator) {
        mSeedGenerator = seedGenerator;
    }

    public void reset() {
        mCustomerArrivalGenerator = new PoissonFlow(mSeedGenerator.generate(), COUNT_OF_CARS_IN_HOUR, ONE_HOUR_TO_SECONDS);
        mNumberOfRepairsGenerator = new PercentGenerator(mSeedGenerator.generate());
        mTypeOfRepairGenerator = new PercentGenerator(mSeedGenerator.generate());

        mEasyRepairTimeGenerator = new DiscreteUniform(mSeedGenerator.generate(), 2, 20);
        mMediumRepairTimeGenerator = new DiscreteEmpirical(mSeedGenerator, Arrays.asList(
                new DiscreteUniformConstruct(10, 40 , 0.1),
                new DiscreteUniformConstruct(41, 61, 0.6),
                new DiscreteUniformConstruct(62, 100, 0.3)
        ));
        mHardRepairTimeGenerator = new DiscreteUniform(mSeedGenerator.generate(), 120, 260);

        mTravelTimeForRepairGenerator = new TriangleUniform(mSeedGenerator.generate(), 120, 540, 240);
        mTravelTimeFromRepairGenerator = new TriangleUniform(mSeedGenerator.generate(), 120, 540, 240);

        mLengthOfOrderAcceptanceGenerator = new ContinuousUniform(mSeedGenerator.generate(), 70, 310);
        mLengthOfCarAcceptanceGenerator = new ContinuousUniform(mSeedGenerator.generate(), 80, 160);
        mLengthOfCarHandoverGenerator = new ContinuousUniform(mSeedGenerator.generate(), 123, 257);
    }

    public int getNumberOfRepairs() {
        final double probabilityForRepairs = mNumberOfRepairsGenerator.generate();

        if (probabilityForRepairs < 0.40) {
            return 1;
        } else if (probabilityForRepairs < 0.55) {
            return 2;
        } else if (probabilityForRepairs < 0.69) {
            return 3;
        } else if (probabilityForRepairs < 0.81) {
            return 4;
        } else if (probabilityForRepairs < 0.90) {
            return 5;
        } else {
            return 6;
        }
    }

    public double getLengthOfRepair() {
        final double probabilityOfType = mTypeOfRepairGenerator.generate();

        if (probabilityOfType < 0.7) {
            return mEasyRepairTimeGenerator.generate() * 60;
        } else if (probabilityOfType < 0.9) {
            return mMediumRepairTimeGenerator.generate() * 60;
        } else {
            return mHardRepairTimeGenerator.generate() * 60;
        }
    }

    public double getTravelTimeForRepair() {
        return mTravelTimeForRepairGenerator.generate();
    }

    public double getTravelTimeFromRepair() {
        return mTravelTimeFromRepairGenerator.generate();
    }

    public double getNextArrival() {
        return mCustomerArrivalGenerator.generate();
    }

    public double getLengthOfOrderAcceptance() {
        return mLengthOfOrderAcceptanceGenerator.generate();
    }

    public double getLengthOfCarAcceptance() {
        return mLengthOfCarAcceptanceGenerator.generate();
    }

    public double getLengthOfCarHandover() {
        return mLengthOfCarHandoverGenerator.generate();
    }
}
