package model.constraint;

/**
 * Created by Tomino on 11/03/16.
 */
public class SlowDownConstraint {

    public static boolean isMax = false;
    public static double mFrequency = 0;
    public static long mSleepTime = 1000;

    public static double getFrequency() {
        return mFrequency;
    }

    public static void setFrequency(final double mFrequency) {
        SlowDownConstraint.mFrequency = mFrequency;
    }

    public static long getSleepTime() {
        return mSleepTime;
    }

    public static void setSleepTime(long mSleepTime) {
        SlowDownConstraint.mSleepTime = mSleepTime;
    }

    public static boolean isMax() {
        return isMax;
    }

    public static void setIsMax(boolean isMax) {
        SlowDownConstraint.isMax = isMax;
    }
}
