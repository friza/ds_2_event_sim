package model.event;

import model.object.Car;
import model.object.Employee;

/**
 * Created by Tomino on 19/03/2017.
 */
public class EmployeeSimulationEvent extends SimulationEvent {

    protected final Employee mEmployee;

    public EmployeeSimulationEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car);

        mEmployee = employee;
    }
}
