package model.event.sysEvent;

import core.Simulation;
import model.event.BaseEvent;

/**
 * Created by Tomino on 11/03/16.
 */
public class StartSimulationEvent extends BaseEvent {

    public StartSimulationEvent(long id, double eventTime) {
        super(id, eventTime);
    }

    @Override
    public void execute(Simulation core) {
        // dummy event
    }
}
