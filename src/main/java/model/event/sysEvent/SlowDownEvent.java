package model.event.sysEvent;

import core.Simulation;
import core.model.Speed;
import model.constraint.SlowDownConstraint;
import model.event.BaseEvent;

/**
 * Created by Tomino on 11/03/16.
 */
public class SlowDownEvent extends BaseEvent {

    public SlowDownEvent(long id, double eventTime) {
        super(id, eventTime);
    }

    @Override
    public void execute(Simulation simulation) {
        if (!SlowDownConstraint.isMax) {
            try {
                Thread.sleep(SlowDownConstraint.getSleepTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (simulation.getSimulationMode() == Speed.SLOW) {
            mEventTime += SlowDownConstraint.getFrequency();
            simulation.planEvent(this);
        }
    }
}
