package model.event;

import core.Simulation;
import model.object.Car;

/**
 * Created by Tomino on 03/04/16.
 */
public class SimulationEvent extends BaseEvent {

    protected final Car mCar;

    public SimulationEvent(long id, double eventTime, Car car) {
        super(id, eventTime);

        mCar = car;
    }

    @Override
    public void execute(Simulation simulation) {
        // implemented in children's
    }
}
