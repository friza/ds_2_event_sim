package model.event.simulation;

import com.sun.javafx.beans.annotations.NonNull;
import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.EmployeeSimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.MoveCar;

/**
 * Created by Tomino on 19/03/2017.
 */
public class ParkCarEndEvent extends EmployeeSimulationEvent {

    private final MoveCar mMoveCar;

    public ParkCarEndEvent(long id, double eventTime, Car car, final Employee employee, final MoveCar moveCar) {
        super(id, eventTime, car, employee);

        mMoveCar = moveCar;
    }

    @Override
    public void execute(Simulation simulation) {
        switch (mMoveCar) {
            case TO_REPAIR:
                if (ReplicationResult.mFreeMechanics.size() > 0) {
                    mCar.setEmployee(ReplicationResult.useMechanic(getEventTime()));
                    // plan CarRepairStartEvent if is some mechanic free (ReplicationResult helper arrays methods)
                    simulation.planEvent(new CarRepairStartEvent(simulation.getEventId(), getEventTime(), mCar, mCar.getEmployee()));
                } else {
                    manageRepairQueueStats();

                    // if not, move car into waiting queue
                    mCar.setStartOfWaiting(getEventTime());
                    mCar.setEmployee(null);

                    ReplicationResult.addCarForRepairQueue(mCar);
                }

                if (ReplicationResult.getForHandoverQueueSize() > 0) {
                    final Car car = ReplicationResult.getFirstCarFromForHandoverQueue();
                    car.setEmployee(mEmployee);

                    manageHandoverQueueStats(car);

                    // if is some car in handover waiting queue, plan ParkCarStartEvent with FROM_REPAIR
                    simulation.planEvent(new ParkCarStartEvent(
                            simulation.getEventId(), getEventTime(), car, mEmployee, MoveCar.FROM_REPAIR));
                } else {
                    // if no car is repaired, go for rest or accept another order
                    if (ReplicationResult.getAcceptanceQueueSize() > 0) {
                        final Car car = ReplicationResult.getFirstCarFromAcceptanceQueue();
                        car.setEmployee(mEmployee);

                        manageAcceptanceQueueStats(car);

                        simulation.planEvent(new OrderAcceptanceEvent(
                                simulation.getEventId(), getEventTime(), car, mEmployee));
                    } else {
                        ReplicationResult.giveBreathToVendor(mEmployee, getEventTime());
                    }
                }
                break;
            case FROM_REPAIR:
                // plan handover of car
                simulation.planEvent(new CarHandoverStartEvent(simulation.getEventId(), getEventTime(), mCar, mEmployee));
                break;
        }
    }

    private void manageHandoverQueueStats(final @NonNull Car car) {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAfterRepairQueueChanged;
        ReplicationResult.mAverageLengthOfForHandoverQueue += timeDivision * ReplicationResult.getForHandoverQueueSize();
        ReplicationResult.mLastCarAfterRepairQueueChanged = getEventTime();

        ReplicationResult.mAverageForHandoverWaitTime += getEventTime() - car.getStartOfWaiting();
    }

    private void manageAcceptanceQueueStats(final @NonNull Car car) {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAcceptanceQueueChanged;
        ReplicationResult.mAverageLengthOfAcceptanceQueue += timeDivision * ReplicationResult.getAcceptanceQueueSize();
        ReplicationResult.mLastCarAcceptanceQueueChanged = getEventTime();

        ReplicationResult.mAverageAcceptanceWaitTime += getEventTime() - car.getStartOfWaiting();
    }

    private void manageRepairQueueStats() {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarBeforeRepairQueueChanged;
        ReplicationResult.mAverageLengthOfForRepairQueue += timeDivision * ReplicationResult.getForRepairQueueSize();
        ReplicationResult.mLastCarBeforeRepairQueueChanged = getEventTime();
    }
}
