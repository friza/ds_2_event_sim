package model.event.simulation;

import core.Simulation;
import model.event.EmployeeSimulationEvent;
import model.event.SimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.CarLocation;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CarHandoverStartEvent extends EmployeeSimulationEvent {

    public CarHandoverStartEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car, employee);
    }

    @Override
    public void execute(Simulation simulation) {
        mCar.setCarLocation(CarLocation.CAR_HANDOVER);

        final double handoverLength = simulation.provideGenerators().getLengthOfCarHandover();
        simulation.planEvent(new CarHandoverEndEvent(simulation.getEventId(), getEventTime() + handoverLength, mCar, mEmployee));
    }
}
