package model.event.simulation;

import core.Simulation;
import model.ReplicationResult;
import model.event.EmployeeSimulationEvent;
import model.event.SimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.CarLocation;
import model.object.enums.MoveCar;

/**
 * Created by Tomino on 19/03/2017.
 */
public class ParkCarStartEvent extends EmployeeSimulationEvent {

    private final MoveCar mMoveCar;

    public ParkCarStartEvent(long id, double eventTime, Car car, final Employee employee, final MoveCar moveCar) {
        super(id, eventTime, car, employee);

        mMoveCar = moveCar;
    }

    @Override
    public void execute(Simulation simulation) {
        double transferLength = 0;

        switch (mMoveCar) {
            case TO_REPAIR:
                mCar.setStartOfCarTakeCare(getEventTime());

                mCar.setCarLocation(CarLocation.ROUTE_TO_SERVICE);
                transferLength = simulation.provideGenerators().getTravelTimeForRepair();
                break;
            case FROM_REPAIR:
                mCar.setCarLocation(CarLocation.ROUTE_FROM_SERVICE);
                transferLength = simulation.provideGenerators().getTravelTimeFromRepair();
                break;
        }

        // plan ParkCarEndEvent of car
        simulation.planEvent(new ParkCarEndEvent(simulation.getEventId(), getEventTime() + transferLength, mCar, mEmployee, mMoveCar));
    }
}
