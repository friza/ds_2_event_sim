package model.event.simulation;

import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.EmployeeSimulationEvent;
import model.event.SimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.CarLocation;

import java.util.ArrayList;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CarRepairStartEvent extends EmployeeSimulationEvent {

    public CarRepairStartEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car, employee);
    }

    @Override
    public void execute(Simulation simulation) {
        ReplicationResult.mForRepairWaitTimeIncremented++;
        mCar.setCarLocation(CarLocation.CAR_REPAIRING);
        mCar.setStartOfRepair(getEventTime());

        // plan CarRepairEndEvent
        simulation.planEvent(new CarRepairEndEvent(simulation.getEventId(), getEventTime() + mCar.getLengthOfRepairs(), mCar, mEmployee));
    }
}
