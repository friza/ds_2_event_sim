package model.event.simulation;

import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.EmployeeSimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.CarLocation;
import model.object.enums.MoveCar;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CarAcceptanceEvent extends EmployeeSimulationEvent {

    public CarAcceptanceEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car, employee);
    }

    @Override
    public void execute(Simulation simulation) {
        ReplicationResult.mAcceptanceWaitTimeIncremented++;
        mCar.setCarLocation(CarLocation.CAR_ACCEPTANCE);

        final double acceptanceLength = simulation.provideGenerators().getLengthOfCarAcceptance();
        simulation.planEvent(new ParkCarStartEvent(simulation.getEventId(), getEventTime() + acceptanceLength, mCar, mEmployee, MoveCar.TO_REPAIR));
    }
}
