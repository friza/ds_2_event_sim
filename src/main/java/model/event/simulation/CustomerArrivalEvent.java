package model.event.simulation;

import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.SimulationEvent;
import model.object.Car;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CustomerArrivalEvent extends SimulationEvent {

    public CustomerArrivalEvent(long id, double eventTime, Car car) {
        super(id, eventTime, car);
    }

    @Override
    public void execute(Simulation simulation) {
        ReplicationResult.addCarToSystem(mCar);

        if (ReplicationResult.mFreeVendors.size() > 0 && ReplicationResult.getAcceptanceQueueSize() == 0) {
            mCar.setEmployee(ReplicationResult.useVendor(getEventTime()));
            // plan OrderAcceptanceEvent if is some vendor free (ReplicationResult helper arrays methods)
            simulation.planEvent(new OrderAcceptanceEvent(simulation.getEventId(), getEventTime(), mCar, mCar.getEmployee()));
        } else {
            manageQueueStats();

            // if not, move car into waiting queue
            mCar.setStartOfWaiting(getEventTime());
            mCar.setEmployee(null);

            ReplicationResult.addCarToAcceptanceQueue(mCar);
        }

        // plan another arrival
        final Car car = simulation.generateCar();
        if (!simulation.getReplicationProcessFlow().willBeInCoolingDown(car.getArrivalTime())) {
            simulation.planEvent(new CustomerArrivalEvent(simulation.getEventId(), car.getArrivalTime(), car));
        }
    }

    private void manageQueueStats() {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAcceptanceQueueChanged;
        ReplicationResult.mAverageLengthOfAcceptanceQueue += timeDivision * ReplicationResult.getAcceptanceQueueSize();
        ReplicationResult.mLastCarAcceptanceQueueChanged = getEventTime();
    }
}
