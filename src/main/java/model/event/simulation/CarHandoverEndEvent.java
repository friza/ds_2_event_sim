package model.event.simulation;

import com.sun.javafx.beans.annotations.NonNull;
import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.EmployeeSimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.MoveCar;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CarHandoverEndEvent extends EmployeeSimulationEvent {

    public CarHandoverEndEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car, employee);
    }

    @Override
    public void execute(Simulation simulation) {
        ReplicationResult.mForHandoverWaitTimeIncremented++;

        manageCarTakeCareStats();

        // save length of car in system
        ReplicationResult.mSumOfAllCustomersTimeInService += getEventTime() - mCar.getArrivalTime();

        // erase car from system
        ReplicationResult.removeCarFromSystem(mCar);

        // plan new acceptance or go rest with vendor
        if (ReplicationResult.getForHandoverQueueSize() > 0) {
            final Car car = ReplicationResult.getFirstCarFromForHandoverQueue();
            car.setEmployee(mEmployee);

            manageHandoverQueueStats(car);

            // if is some car in handover waiting queue, plan ParkCarStartEvent with FROM_REPAIR
            simulation.planEvent(new ParkCarStartEvent(
                    simulation.getEventId(), getEventTime(), car, mEmployee, MoveCar.FROM_REPAIR));
        } else {
            // if no car is repaired, go for rest or accept another order
            if (ReplicationResult.getAcceptanceQueueSize() > 0) {
                final Car car = ReplicationResult.getFirstCarFromAcceptanceQueue();
                car.setEmployee(mEmployee);

                manageAcceptanceQueueStats(car);

                simulation.planEvent(new OrderAcceptanceEvent(
                        simulation.getEventId(), getEventTime(), car, mEmployee));
            } else {
                ReplicationResult.giveBreathToVendor(mEmployee, getEventTime());
            }
        }
    }

    private void manageCarTakeCareStats() {
        ReplicationResult.mSumOfAllCustomerTimeWaitingForCar += getEventTime() - mCar.getStartOfCarTakeCare();
    }

    private void manageHandoverQueueStats(final @NonNull Car car) {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAfterRepairQueueChanged;
        ReplicationResult.mAverageLengthOfForHandoverQueue += timeDivision * ReplicationResult.getForHandoverQueueSize();
        ReplicationResult.mLastCarAfterRepairQueueChanged = getEventTime();

        ReplicationResult.mAverageForHandoverWaitTime += getEventTime() - car.getStartOfWaiting();
    }

    private void manageAcceptanceQueueStats(final @NonNull Car car) {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAcceptanceQueueChanged;
        ReplicationResult.mAverageLengthOfAcceptanceQueue += timeDivision * ReplicationResult.getAcceptanceQueueSize();
        ReplicationResult.mLastCarAcceptanceQueueChanged = getEventTime();

        ReplicationResult.mAverageAcceptanceWaitTime += getEventTime() - car.getStartOfWaiting();
    }
}
