package model.event.simulation;

import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.SimulationEvent;
import model.object.Car;

import java.util.ArrayList;

/**
 * Created by Tomino on 28/03/2017.
 */
public class EndOfDayEvent extends SimulationEvent {

    public EndOfDayEvent(long id, double eventTime, Car car) {
        super(id, eventTime, car);
    }

    @Override
    public void execute(Simulation simulation) {
        // save count of erased customers
        ReplicationResult.mCountOfFiredCustomers += ReplicationResult.getAcceptanceQueueSize();

        // save stats about clearing
        /*final double timeDivision = getEventTime() - ReplicationResult.mLastCarAcceptanceQueueChanged;
        ReplicationResult.mAverageLengthOfAcceptanceQueue += timeDivision * ReplicationResult.getAcceptanceQueueSize();
        ReplicationResult.mLastCarAcceptanceQueueChanged = getEventTime();

        final ArrayList<Car> cars = new ArrayList(ReplicationResult.mCarAcceptanceQueue);
        for (Car car : cars) {
            ReplicationResult.mAverageAcceptanceWaitTime += getEventTime() - car.getStartOfWaiting();
        }*/

        // erase of acceptance waiting
        ReplicationResult.clearAcceptanceQueue();

        final EndOfDayEvent endOfDayEvent = simulation.generateEndOfDayEvent();
        if (!simulation.getReplicationProcessFlow().willBeInCoolingDown(endOfDayEvent.getEventTime())) {
            simulation.planEvent(simulation.generateEndOfDayEvent());
        }
    }
}
