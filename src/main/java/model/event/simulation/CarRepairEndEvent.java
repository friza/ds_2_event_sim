package model.event.simulation;

import com.sun.javafx.beans.annotations.NonNull;
import core.Simulation;
import model.ReplicationResult;
import model.SimulationResult;
import model.event.EmployeeSimulationEvent;
import model.object.Car;
import model.object.Employee;
import model.object.enums.MoveCar;

/**
 * Created by Tomino on 19/03/2017.
 */
public class CarRepairEndEvent extends EmployeeSimulationEvent {

    public CarRepairEndEvent(long id, double eventTime, Car car, final Employee employee) {
        super(id, eventTime, car, employee);
    }

    @Override
    public void execute(Simulation simulation) {
        manageLengthOfRepairStats();
        manageHandoverQueueStats();
        mCar.carIsRepaired(true);

        if (ReplicationResult.mFreeVendors.size() > 0) {
            // do handover
            mCar.setEmployee(ReplicationResult.useVendor(getEventTime()));

            simulation.planEvent(new ParkCarStartEvent(
                    simulation.getEventId(), getEventTime(), mCar, mCar.getEmployee(), MoveCar.FROM_REPAIR));
        } else {
            // move car into handover queue, null employee
            mCar.setStartOfWaiting(getEventTime());
            mCar.setEmployee(null);

            ReplicationResult.addCarForHandoverQueue(mCar);
        }

        // if is some car waiting for repair, repair it
        if (ReplicationResult.getForRepairQueueSize() > 0) {
            final Car car = ReplicationResult.getFirstCarFromForRepairQueue();
            car.setEmployee(mEmployee);

            manageRepairQueueStats(car);

            simulation.planEvent(new CarRepairStartEvent(
                    simulation.getEventId(), getEventTime(), car, mEmployee));
        } else {
            ReplicationResult.giveBreatheToMechanic(mEmployee, getEventTime());
        }
    }

    private void manageLengthOfRepairStats() {
        ReplicationResult.mRepairedCars++;
        ReplicationResult.mSumOfAllLengthsOfRepair += mCar.getLengthOfRepairs();
    }

    private void manageRepairQueueStats(final @NonNull Car car) {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarBeforeRepairQueueChanged;
        ReplicationResult.mAverageLengthOfForRepairQueue += timeDivision * ReplicationResult.getForRepairQueueSize();
        ReplicationResult.mLastCarBeforeRepairQueueChanged = getEventTime();

        ReplicationResult.mAverageForRepairWaitTime += getEventTime() - car.getStartOfWaiting();
    }

    private void manageHandoverQueueStats() {
        final double timeDivision = getEventTime() - ReplicationResult.mLastCarAfterRepairQueueChanged;
        ReplicationResult.mAverageLengthOfForHandoverQueue += timeDivision * ReplicationResult.getForHandoverQueueSize();
        ReplicationResult.mLastCarAfterRepairQueueChanged = getEventTime();
    }
}
