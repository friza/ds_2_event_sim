package model.event;

import core.Simulation;

/**
 * Created by Tomino on 11/03/16.
 */
public abstract class BaseEvent implements Comparable<BaseEvent> {

    private final long ID;
    protected double mEventTime;

    public BaseEvent(final long id, final double eventTime) {
        ID = id;
        mEventTime = eventTime;
    }

    public abstract void execute(final Simulation simulation);

    public void recalculateEventTime(final double maxReplicationTime) {
        mEventTime = mEventTime - maxReplicationTime;
    }

    @Override
    public int compareTo(BaseEvent event) {
        if ((event.getEventTime() < getEventTime())) {
            return 1;
        } else if ((event.getEventTime() > getEventTime())) {
            return -1;
        } else {
            if (event.ID < ID) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public double getEventTime() {
        return mEventTime;
    }
}
