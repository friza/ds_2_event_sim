package model;

import model.object.Car;
import model.object.Employee;
import model.object.enums.CarLocation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Tomino on 27/03/16.
 */
public class ReplicationResult {

    public static long mCustomerCount;
    public static double mCountOfFiredCustomers;

    public static double mSumOfAllCustomersTimeInService;
    public static double mSumOfAllCustomerTimeWaitingForCar;
    public static double mSumOfAllLengthsOfRepair;

    public static double mAverageLengthOfAcceptanceQueue;
    public static double mAverageLengthOfForRepairQueue;
    public static double mAverageLengthOfForHandoverQueue;

    public static double mAverageAcceptanceWaitTime;
    public static long mAcceptanceWaitTimeIncremented;
    public static double mAverageForRepairWaitTime;
    public static long mForRepairWaitTimeIncremented;
    public static double mAverageForHandoverWaitTime;
    public static long mForHandoverWaitTimeIncremented;

    public static double mAverageLengthOfFreeVendorQueue;
    public static double mAverageLengthOfFreeMechanicsQueue;

    public static double mRepairedCars;

    public static ArrayList<Car> mCarsInSystem;

    public static Queue<Employee> mFreeVendors;
    public static Queue<Employee> mFreeMechanics;

    public static Queue<Car> mCarAcceptanceQueue;
    public static Queue<Car> mCarForRepairQueue;
    public static Queue<Car> mCarForHandoverQueue;

    public static double mReplicationTime;

    public static double mLastCarAcceptanceQueueChanged;
    public static double mLastCarBeforeRepairQueueChanged;
    public static double mLastCarAfterRepairQueueChanged;

    public static double mLastFreeVendorsQueueChanged;
    public static double mLastFreeMechanicsQueueChanged;

    public static void init() {
        mCarAcceptanceQueue = new LinkedList<>();
        mCarForRepairQueue = new LinkedList<>();
        mCarForHandoverQueue = new LinkedList<>();

        mFreeVendors = new LinkedList<>();
        mFreeMechanics = new LinkedList<>();

        mCarsInSystem = new ArrayList<>();

        reset();
    }

    public static void reset() {
        mCustomerCount = 0;
        mCountOfFiredCustomers = 0.0;

        mSumOfAllCustomersTimeInService = 0.0;
        mSumOfAllCustomerTimeWaitingForCar = 0.0;
        mSumOfAllLengthsOfRepair = 0.0;

        mRepairedCars = 0.0;

        mCarAcceptanceQueue.clear();
        mCarForRepairQueue.clear();
        mCarForHandoverQueue.clear();

        mFreeMechanics.clear();
        mFreeVendors.clear();

        mCarsInSystem.clear();

        mLastCarAcceptanceQueueChanged = 0.0;
        mLastCarBeforeRepairQueueChanged = 0.0;
        mLastCarAfterRepairQueueChanged = 0.0;

        mLastFreeVendorsQueueChanged = 0.0;
        mLastFreeMechanicsQueueChanged = 0.0;

        mReplicationTime = 0.0;

        mAverageAcceptanceWaitTime = 0.0;
        mAcceptanceWaitTimeIncremented = 0;
        mAverageForRepairWaitTime = 0.0;
        mForRepairWaitTimeIncremented = 0;
        mAverageForHandoverWaitTime = 0.0;
        mForHandoverWaitTimeIncremented= 0;

        mAverageLengthOfFreeVendorQueue = 0.0;
        mAverageLengthOfFreeMechanicsQueue = 0.0;

        mAverageLengthOfAcceptanceQueue = 0.0;
        mAverageLengthOfForRepairQueue = 0.0;
        mAverageLengthOfForHandoverQueue = 0.0;
    }

    public static void giveBreathToVendor(final Employee employee, final double eventTime) {
        manageVendorQueueLengthStat(eventTime);

        employee.setOccupation(false);
        mFreeVendors.add(employee);
    }

    public static Employee useVendor(final double eventTime) {
        manageVendorQueueLengthStat(eventTime);

        final Employee employee = mFreeVendors.poll();
        employee.setOccupation(true);

        return employee;
    }

    private static void manageVendorQueueLengthStat(final double eventTime) {
        final double timeDivision = eventTime - ReplicationResult.mLastFreeVendorsQueueChanged;
        mAverageLengthOfFreeVendorQueue += timeDivision * ReplicationResult.mFreeVendors.size();
        ReplicationResult.mLastFreeVendorsQueueChanged = eventTime;
    }

    public static void giveBreatheToMechanic(final Employee employee, final double eventTime) {
        manageMechanicsQueueLengthStat(eventTime);

        employee.setOccupation(false);
        mFreeMechanics.add(employee);
    }

    public static Employee useMechanic(final double eventTime) {
        manageMechanicsQueueLengthStat(eventTime);

        final Employee employee = mFreeMechanics.poll();
        employee.setOccupation(true);

        return employee;
    }

    private static void manageMechanicsQueueLengthStat(final double eventTime) {
        final double timeDivision = eventTime - ReplicationResult.mLastFreeMechanicsQueueChanged;
        mAverageLengthOfFreeMechanicsQueue += timeDivision * ReplicationResult.mFreeMechanics.size();
        ReplicationResult.mLastFreeMechanicsQueueChanged = eventTime;
    }

    public static void addCarToSystem(final Car car) {
        mCustomerCount++;
        mCarsInSystem.add(car);
    }

    public static void removeCarFromSystem(final Car car) { // use when car will be handover to customer after repair
        mCarsInSystem.remove(car);
    }

    public static ArrayList<Car> getAllCarsFromSystem() {
        return mCarsInSystem;
    }

    public static void addCarToAcceptanceQueue(final Car car) {
        car.setCarLocation(CarLocation.WAITING_FOR_ORDER_ACCEPTANCE);
        mCarAcceptanceQueue.add(car);
    }

    public static Car getFirstCarFromAcceptanceQueue() {
        return mCarAcceptanceQueue.poll();
    }

    public static int getAcceptanceQueueSize() {
        return mCarAcceptanceQueue.size();
    }

    public static void clearAcceptanceQueue() {
        mCarsInSystem.removeAll(mCarAcceptanceQueue);
        mCustomerCount -= getAcceptanceQueueSize();
        mCarAcceptanceQueue.clear();
    }

    public static void addCarForRepairQueue(final Car car) {
        car.setCarLocation(CarLocation.WAITING_FOR_REPAIR);
        mCarForRepairQueue.add(car);
    }

    public static Car getFirstCarFromForRepairQueue() {
        return mCarForRepairQueue.poll();
    }

    public static int getForRepairQueueSize() {
        return mCarForRepairQueue.size();
    }

    public static void addCarForHandoverQueue(final Car car) {
        car.setCarLocation(CarLocation.WAITING_FOR_HANDOVER);
        mCarForHandoverQueue.add(car);
    }

    public static Car getFirstCarFromForHandoverQueue() {
        return mCarForHandoverQueue.poll();
    }

    public static int getForHandoverQueueSize() {
        return mCarForHandoverQueue.size();
    }

    public static double getAverageCustomerTimeInService() {
        return mSumOfAllCustomersTimeInService / mCustomerCount - mCarsInSystem.size();
    }

    public static double getAverageCustomerWaitingForCar() {
        return mSumOfAllCustomerTimeWaitingForCar / mCustomerCount - mCarsInSystem.size();
    }

    public static double getAverageRepairLength() {
        return mSumOfAllLengthsOfRepair / mRepairedCars;
    }
}
