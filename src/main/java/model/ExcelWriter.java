package model;

import com.sun.javafx.beans.annotations.NonNull;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.DecimalFormat;

/**
 * Created by Tomino on 01/04/2017.
 */
public class ExcelWriter {

    private final String mFileName;

    public ExcelWriter(final @NonNull String fileName) {
        mFileName = fileName;
    }

    public void writeLineToExcel(final int iteration, final int vendors, final int mechanics) {
        XSSFWorkbook workbook;
        XSSFSheet sheet;

        //  if file not exists, create him
        try {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(new File(mFileName));
            } catch (FileNotFoundException e) {
                System.out.print("Creating new file " + mFileName);

                workbook = new XSSFWorkbook();
                sheet = workbook.createSheet("Simulation data");

                // create header
                Row row = sheet.createRow(0);

                Cell cell = row.createCell(0);
                cell.setCellValue("Experiment");

                cell = row.createCell(1);
                cell.setCellValue("Vendors");
                cell = row.createCell(2);
                cell.setCellValue("Mechanics");

                cell = row.createCell(3);
                cell.setCellValue("Time of car in system - h");
                cell = row.createCell(4);
                cell.setCellValue("Time of car in service - h");
                cell = row.createCell(5);
                cell.setCellValue("Time of repair - h");

                cell = row.createCell(6);
                cell.setCellValue("Av free vendors");
                cell = row.createCell(7);
                cell.setCellValue("Av free mechanics");
                cell = row.createCell(8);
                cell.setCellValue("Av fired customer");

                cell = row.createCell(9);
                cell.setCellValue("Av length of acceptance queue");
                cell = row.createCell(10);
                cell.setCellValue("Av length of repair queue");
                cell = row.createCell(11);
                cell.setCellValue("Av length of handover queue");

                cell = row.createCell(12);
                cell.setCellValue("Av wait it acceptance queue - m");
                cell = row.createCell(13);
                cell.setCellValue("Av wait in repair queue - m");
                cell = row.createCell(14);
                cell.setCellValue("Av wait in handover queue - m");

                cell = row.createCell(15);
                cell.setCellValue("Av of cooling - h");

                FileOutputStream out = new FileOutputStream(new File(mFileName));
                workbook.write(out);
                out.close();

                fis = new FileInputStream(new File(mFileName));
            }

            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheetAt(0);

            // append new line of records
            if (fis != null) {
                Row row = sheet.createRow(iteration);

                Cell cell = row.createCell(0);
                cell.setCellValue(iteration);

                cell = row.createCell(1);
                cell.setCellValue(vendors);
                cell = row.createCell(2);
                cell.setCellValue(mechanics);

                cell = row.createCell(3);
                cell.setCellValue(toHours(SimulationResult.getAverageCustomerTimeInService()));
                cell = row.createCell(4);
                cell.setCellValue(toHours(SimulationResult.getAverageCustomerWaitingForCar()));
                cell = row.createCell(5);
                cell.setCellValue(toHours(SimulationResult.getAverageLengthOfRepair()));

                cell = row.createCell(6);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageLengthOfFreeVendorQueue()));
                cell = row.createCell(7);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageLengthOfFreeMechanicsQueue()));
                cell = row.createCell(8);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageCountOfFiredCustomers()));

                cell = row.createCell(9);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageLengthOfAcceptanceQueue()));
                cell = row.createCell(10);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageLengthOfForRepairQueue()));
                cell = row.createCell(11);
                cell.setCellValue(getDecimalFormat(SimulationResult.getAverageLengthOfForHandoverQueue()));

                cell = row.createCell(12);
                cell.setCellValue(toMinutes(SimulationResult.getAverageAcceptanceWaitTime()));
                cell = row.createCell(13);
                cell.setCellValue(toMinutes(SimulationResult.getAverageForRepairWaitTime()));
                cell = row.createCell(14);
                cell.setCellValue(toMinutes(SimulationResult.getAverageForHandoverWaitTime()));

                cell = row.createCell(15);
                cell.setCellValue(toHours(SimulationResult.getAverageCoolingUpLength()));

                // save this line
                FileOutputStream out = new FileOutputStream(new File(mFileName));
                workbook.write(out);
                out.close();
            }
        } catch (IOException ioEx) {

        }
    }

    private String getRealTime(final double timeInSeconds) {
        final int hours = (int) Math.floor(timeInSeconds / 3600) % 8;
        int minutes = (int) Math.floor((timeInSeconds % 3600) / 60);
        int seconds = (int) Math.floor(timeInSeconds % 60);

        final int day = (int) Math.floor((timeInSeconds / 3600) / 8);

        if (minutes < 10 & seconds < 10) {
            return day + " day, " + (hours + 7) + ":0" + minutes + ":0" + seconds;
        } else if (minutes < 10) {
            return day + " day, " + (hours + 7) + ":0" + minutes + ":" + seconds;
        } else if (seconds < 10) {
            return day + " day, " + (hours + 7) + ":" + minutes + ":0" + seconds;
        } else {
            return day + " day, " + (hours + 7) + ":" + minutes + ":" + seconds;
        }
    }

    private String toHours(final double timeInSeconds) {
        return getDecimalFormat(timeInSeconds / 60 / 60);
    }

    private String toMinutes(final double timeInSeconds) {
        return getDecimalFormat(timeInSeconds / 60);
    }

    private static String getDecimalFormat(final double value) {
        DecimalFormat df = new DecimalFormat("#.######");
        return df.format(value).toString();
    }

}
