package model.object;

import model.GeneratorModule;
import model.object.enums.CarLocation;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Tomino on 12/03/16.
 */
public class Car {

    private final static int ONE_HOUR = 60;

    private final long mId;
    private Queue<Double> mRepairTimeArray; // time in seconds --- one item, is one repair with param time

    private Employee mEmployee = null;
    private CarLocation mCarLocation = CarLocation.ARRIVAL;

    private double mArrivalTime;
    private double mStartOfCarTakeCare = 0;
    private double mStartOfRepair = 0;
    private double mStartOfWaiting = 0;

    private boolean mCarRepaired = false;

    public Car(final long id, final double arrivalTime, final GeneratorModule generatorModule) {
        mId = id;
        mArrivalTime = arrivalTime;

        initRepairTimeArray(generatorModule);
    }

    public void initRepairTimeArray(final GeneratorModule generatorModule) {
        mRepairTimeArray = new LinkedList<>();

        final int countOfRepairs = generatorModule.getNumberOfRepairs();
        for (int i = 0; i < countOfRepairs; i++) {
            mRepairTimeArray.add(generatorModule.getLengthOfRepair());
        }
    }

    public long getCarID() {
        return mId;
    }

    public double getLengthOfRepairs() {
        double result = 0;

        for (Double aDouble : mRepairTimeArray) {
            result += aDouble;
        }

        return result;
    }

    public int getCountOfRepairs() {
        return mRepairTimeArray.size();
    }

    /**
     * CAN BE NULL, IF CAR IS ALREADY REPAIRED !!!
     *
     * @return duration of repair or null, if car is repaired already
     */
    public double getNextRepair() {
        return mRepairTimeArray.poll();
    }

    public CarLocation getCarLocation() {
        return mCarLocation;
    }

    public void setCarLocation(CarLocation carLocation) {
        this.mCarLocation = carLocation;
    }

    public double getStartOfWaiting() {
        return mStartOfWaiting;
    }

    public void setStartOfWaiting(double mStartOfWaiting) {
        this.mStartOfWaiting = mStartOfWaiting;
    }

    public double getStartOfRepair() {
        return mStartOfRepair;
    }

    public void setStartOfRepair(double mStartOfRepair) {
        this.mStartOfRepair = mStartOfRepair;
    }

    public double getArrivalTime() {
        return mArrivalTime;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee mEmployee) {
        this.mEmployee = mEmployee;
    }

    public void carIsRepaired(final boolean carIsRepaired) {
        mCarRepaired = carIsRepaired;
    }

    public double getStartOfCarTakeCare() {
        return mStartOfCarTakeCare;
    }

    public void setStartOfCarTakeCare(double mStartOfCarTakeCare) {
        this.mStartOfCarTakeCare = mStartOfCarTakeCare;
    }

    public String getPercentageOfRepair(final double actualReplicationTime) {
        if (0 == getStartOfRepair()) {
            return "0 %";
        } else if (mCarRepaired) {
            return "100 %";
        } else {
            final double result = ((actualReplicationTime - getStartOfRepair()) * 100) / getLengthOfRepairs();
            return getDecimalFormat(result) + " %";
        }
    }

    private static String getDecimalFormat(final double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value).toString();
    }
}
