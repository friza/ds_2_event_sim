package model.object;

import model.object.enums.EmployeeType;

/**
 * Created by Tomino on 19/03/2017.
 */
public class Employee {

    private final long mId;
    private final EmployeeType mEmployeeType;

    private boolean mOccupation;

    public Employee(final EmployeeType employeeType, final long id) {
        mId = id;
        mEmployeeType = employeeType;

        mOccupation = false;
    }

    public EmployeeType getEmployeeType() {
        return mEmployeeType;
    }

    public void setOccupation(final boolean occupation) {
        mOccupation = occupation;
    }

    public boolean getOccupation() {
        return mOccupation;
    }
}
