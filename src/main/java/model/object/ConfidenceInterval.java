package model.object;

/**
 * Created by Tomino on 03/04/16.
 */
public class ConfidenceInterval {

    private static double esSquare(final double normalSum, final double squareSum, final long mCountOfReplication) {
        return Math.sqrt((squareSum / mCountOfReplication) - Math.pow(normalSum / mCountOfReplication, 2));
    }

    public static double getMinConfidenceInterval(final double normalSum, final double squareSum, final long mCountOfReplication) {
        return round((normalSum / mCountOfReplication) - ((/*1.96*/1.645 * esSquare(normalSum, squareSum, mCountOfReplication)) / Math.sqrt(mCountOfReplication - 1)), 4);
    }

    public static double getMaxConfidenceInterval(final double normalSum, final double squareSum, final long mCountOfReplication) {
        return round((normalSum / mCountOfReplication) + ((/*1.96*/1.645 * esSquare(normalSum, squareSum, mCountOfReplication)) / Math.sqrt(mCountOfReplication - 1)), 4);
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        final long factor = (long) Math.pow(10, places);
        value = value * factor;
        final long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
