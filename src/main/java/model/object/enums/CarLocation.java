package model.object.enums;

/**
 * Created by Tomino on 10/04/16.
 */
public enum CarLocation {
    ARRIVAL("Arrival of car"),
    WAITING_FOR_ORDER_ACCEPTANCE("Waiting for order acceptance"),
    ORDER_ACCEPTANCE("Acceptance of order"),
    CAR_ACCEPTANCE("Acceptance of car"),
    ROUTE_TO_SERVICE("Route to service"),
    WAITING_FOR_REPAIR("Waiting for repair"),
    CAR_REPAIRING("Car repairing"),
    WAITING_FOR_HANDOVER("Waiting for customer handover"),
    ROUTE_FROM_SERVICE("Route from service"),
    CAR_HANDOVER("Handover of car to customer");

    private final String mName;
    CarLocation(final String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return mName;
    }
}
