package model.object.enums;

/**
 * Created by Tomino on 27/03/16.
 */
public enum MoveCar {
    TO_REPAIR("to_repair"),
    FROM_REPAIR("from_repair");

    private final String mName;
    MoveCar(final String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return mName;
    }
}
