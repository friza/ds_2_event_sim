package model.object.enums;

/**
 * Created by Tomino on 19/03/2017.
 */
public enum EmployeeType {
    VENDOR("Vendor"),
    MECHANIC("Mechanic");

    private final String mName;
    EmployeeType(final String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return mName;
    }
}
