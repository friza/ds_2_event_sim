package model.object;

/**
 * Created by Tomino on 01/04/2017.
 */
public class ReplicationProcessFlow {

    private Flow mFlow = Flow.NORMAL;

    public double mActualTimeOfReplication;
    public final double mMaxTimeOfReplication;

    public ReplicationProcessFlow(final double maxTimeOfReplication) {
        mMaxTimeOfReplication = maxTimeOfReplication;
    }

    /**
     * reset in reset of simulation
     */
    public void reset() {
        mFlow = Flow.NORMAL;
        mActualTimeOfReplication = 0.0;
    }

    /**
     * Check every event obtaining
     * @param actualTimeOfReplication
     */
    public void checkSwitch(final double actualTimeOfReplication) {
        setActualTimeOfReplication(actualTimeOfReplication);

        if (mActualTimeOfReplication >= mMaxTimeOfReplication) {
            coolUpReplication();
        }
    }

    private void setActualTimeOfReplication(final double actualTimeOfReplication) {
        mActualTimeOfReplication = actualTimeOfReplication;
    }

    private void coolUpReplication() {
        mFlow = Flow.COOLING_DOWN;
    }

    /**
     * Call this for stats in save results
     * @return length of cooling down of replication
     */
    public double getCoolingUpLength() {
        return mActualTimeOfReplication - mMaxTimeOfReplication;
    }

    /**
     * Check this after checkSwitch call in simulation for process diff - if used willBeInCoolingDown(..) this can be skipped
     * @return if flow is COOLING_DOWN stop CustomerArrivalEvent & EndOfDayEvent
     */
    public Flow getFlow() {
        return mFlow;
    }

    /**
     * Use it for planing  CustomerArrivalEvent & EndOfDayEvent
     * @param timeOfEvent when it may be planned
     * @return if may be planned
     */
    public boolean willBeInCoolingDown(final double timeOfEvent) {
        return timeOfEvent >= mMaxTimeOfReplication;
    }

    public enum Flow {
        NORMAL, COOLING_DOWN
    }
}
