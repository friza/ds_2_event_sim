package model;

/**
 * Created by Tomino on 03/04/16.
 */
public class SimulationResult {

    public static double mAverageCustomerTimeInService;
    public static double mAverageCustomerTimeInServiceSquare;
    public static double mAverageCustomerWaitingForCarSquare;
    public static double mAverageCustomerWaitingForCar;
    public static double mAverageLengthOfRepair;
    public static double mAverageLengthOfRepairSquare;

    public static double mAverageCountOfFiredCustomers;
    public static double mAverageCountOfFiredCustomersSquare;

    public static double mAverageLengthOfAcceptanceQueue;
    public static double mAverageLengthOfAcceptanceQueueSquare;
    public static double mAverageLengthOfForRepairQueue;
    public static double mAverageLengthOfForRepairQueueSquare;
    public static double mAverageLengthOfForHandoverQueue;
    public static double mAverageLengthOfForHandoverQueueSquare;

    public static double mAverageLengthOfFreeVendorQueue;
    public static double mAverageLengthOfFreeVendorQueueSquare;
    public static double mAverageLengthOfFreeMechanicsQueue;
    public static double mAverageLengthOfFreeMechanicsQueueSquare;

    public static double mAverageCoolingUpLength;
    public static double mAverageCoolingUpLengthSquare;

    public static double mAverageAcceptanceWaitTime;
    public static double mAverageAcceptanceWaitTimeSquare;
    public static double mAverageForRepairWaitTime;
    public static double mAverageForRepairWaitTimeSquare;
    public static double mAverageForHandoverWaitTime;
    public static double mAverageForHandoverWaitTimeSquare;

    public static long mCountOfReplication;
    public static double mReplicationLength;

    public static void init(final double replicationLength) {
        mCountOfReplication = 0L;
        mReplicationLength = replicationLength;

        mAverageCustomerTimeInService = 0.0;
        mAverageCustomerTimeInServiceSquare = 0.0;
        mAverageCustomerWaitingForCarSquare = 0.0;
        mAverageCustomerWaitingForCar = 0.0;
        mAverageLengthOfRepair = 0.0;
        mAverageLengthOfRepairSquare = 0.0;

        mAverageCountOfFiredCustomers = 0.0;
        mAverageCountOfFiredCustomersSquare = 0.0;

        mAverageLengthOfForRepairQueue = 0.0;
        mAverageLengthOfForRepairQueueSquare = 0.0;
        mAverageLengthOfAcceptanceQueue = 0.0;
        mAverageLengthOfAcceptanceQueueSquare = 0.0;
        mAverageLengthOfForHandoverQueue = 0.0;
        mAverageLengthOfForHandoverQueueSquare = 0.0;

        mAverageLengthOfFreeVendorQueue = 0.0;
        mAverageLengthOfFreeVendorQueueSquare = 0.0;
        mAverageLengthOfFreeMechanicsQueue = 0.0;
        mAverageLengthOfFreeMechanicsQueueSquare = 0.0;

        mAverageCoolingUpLength = 0.0;
        mAverageCoolingUpLengthSquare = 0.0;

        mAverageAcceptanceWaitTime = 0.0;
        mAverageAcceptanceWaitTimeSquare = 0.0;
        mAverageForRepairWaitTime = 0.0;
        mAverageForRepairWaitTimeSquare = 0L;
        mAverageForHandoverWaitTime = 0.0;
        mAverageForHandoverWaitTimeSquare = 0L;
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        final long factor = (long) Math.pow(10, places);
        value = value * factor;
        final long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    // --- CONFIDENCE INTERVAL ---

    private static double esSquare(final double normalSum, final double squareSum) {
        return Math.sqrt((squareSum / mCountOfReplication) - Math.pow(normalSum / mCountOfReplication, 2));
    }

    public static double getMinConfidenceInterval(final double normalSum, final double squareSum) {
        return round((normalSum / mCountOfReplication) - ((/*1.96*/1.645 * esSquare(normalSum, squareSum)) / Math.sqrt(mCountOfReplication - 1)), 4);
    }

    public static double getMaxConfidenceInterval(final double normalSum, final double squareSum) {
        return round((normalSum / mCountOfReplication) + ((/*1.96*/1.645 * esSquare(normalSum, squareSum)) / Math.sqrt(mCountOfReplication - 1)), 4);
    }

    // --- OTHER STATISTICS ---

    public static long getCountOfReplication() {
        return mCountOfReplication;
    }

    public static double getAverageCustomerTimeInService() {
        return mAverageCustomerTimeInService / mCountOfReplication;
    }

    public static double getAverageLengthOfAcceptanceQueue() {
        return mAverageLengthOfAcceptanceQueue / mCountOfReplication;
    }

    public static double getAverageLengthOfForRepairQueue() {
        return mAverageLengthOfForRepairQueue / mCountOfReplication;
    }

    public static double getAverageLengthOfForHandoverQueue() {
        return mAverageLengthOfForHandoverQueue / mCountOfReplication;
    }

    public static double getAverageAcceptanceWaitTime() {
        return mAverageAcceptanceWaitTime / mCountOfReplication;
    }

    public static double getAverageForRepairWaitTime() {
        return mAverageForRepairWaitTime / mCountOfReplication;
    }

    public static double getAverageForHandoverWaitTime() {
        return mAverageForHandoverWaitTime / mCountOfReplication;
    }

    public static double getAverageCountOfFiredCustomers() {
        return mAverageCountOfFiredCustomers / mCountOfReplication;
    }

    public static double getAverageCustomerWaitingForCar() {
        return mAverageCustomerWaitingForCar / mCountOfReplication;
    }

    public static double getAverageLengthOfRepair() {
        return mAverageLengthOfRepair / mCountOfReplication;
    }

    public static double getAverageLengthOfFreeVendorQueue() {
        return mAverageLengthOfFreeVendorQueue / mCountOfReplication;
    }

    public static double getAverageLengthOfFreeMechanicsQueue() {
        return mAverageLengthOfFreeMechanicsQueue / mCountOfReplication;
    }

    public static double getAverageCoolingUpLength() {
        return mAverageCoolingUpLength / mCountOfReplication;
    }
}
