package ui;

import core.IForm;
import core.Simulation;
import core.model.Speed;
import core.model.Type;
import model.ExcelWriter;
import model.ReplicationResult;
import model.SimulationResult;
import model.constraint.SlowDownConstraint;
import model.object.Car;
import model.object.ConfidenceInterval;
import model.object.Employee;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Tomino on 11/03/16.
 */
public class MainForm implements IForm {

    private static final int SLIDER_MAX_VALUE = 1000;
    private static final long MAX_REPLICATION_TIME = 90 * 8 * 60 * 60;

    private final static int vendor_min = 1;
    private final static int vendor_max = 10;

    private final static int mechanic_min = 16;
    private final static int mechanic_max = 26;

    private JPanel root;

    private JButton btnStartSim;
    private JLabel lblSimStatus;
    private JLabel lblCountOfReplications;
    private JButton btnStopSim;
    private JTextField txtCountOfReplications;
    private JTabbedPane tabbedPane1;
    private JPanel panelSettlingChart;
    private JPanel panelReplStatistics;
    private JButton btnPauseOrResumeSim;
    private JLabel lblAverageQueueLengthAcceptance;
    private JLabel lblAverageQueueLengthHandover;
    private JLabel lblAverageQueueLengthForRepair;
    private JLabel lblAverageWaitAcceptance;
    private JLabel lblAverageWaitForRepair;
    private JLabel lblCurrentAcceptanceQueue;
    private JLabel lblCurrentForRepairQueue;
    private JSlider slider1;
    private JTextArea textArea1;
    private JRadioButton SLOWRadioButton;
    private JLabel lblReplTime;
    private JTextField txtGraphWarmup;
    private JTextField txtSimulationWarmup;
    private JLabel lblAverageTimeOfCarInSystem;
    private JLabel lblAverageWaitForHandover;
    private JLabel lblCurrentForHandoverQueue;
    private JLabel lblCountOfFreeVendors;
    private JLabel lblCountOfFreeMechanics;
    private JTextField txtCountOfMechanics;
    private JTextField txtCountOfVendors;
    private JLabel lblAverageFiredCustomers;
    private JLabel lblAverageTimeOfCarInService;
    private JLabel lblAverageTimeOfRepair;
    private JLabel lblAverageCountOfFreeVendors;
    private JLabel lblAverageCountOfFreeMechanics;
    private JLabel lblAverageCoolingLength;
    private JButton btnTestingScenarios;
    private JTextField txtGraphPeriod;
    private JPanel panelSimStatistics;
    private JPanel panelVendorChart;
    private JPanel panelMechanicChart;
    private JRadioButton radioMechanic;
    private JRadioButton radioBoth;
    private JRadioButton radioVendor;
    private JLabel lblAverageTimeOfCarInServiceIS;
    private JLabel lblAverageWaitAcceptanceIS;
    private JLabel lblAverageFiredCustomersIS;
    private JLabel lblAverageTimeOfRepairIS;
    private JLabel lblAverageTimeOfCarInSystemIS;
    private JLabel lblAverageCoolingLengthIS;
    private JLabel lblAverageCountOfFreeVendorsIS;
    private JLabel lblAverageCountOfFreeMechanicsIS;
    private JLabel lblAverageQueueLengthAcceptanceIS;
    private JLabel lblAverageQueueLengthForRepairIS;
    private JLabel lblAverageQueueLengthHandoverIS;
    private JLabel lblAverageWaitForRepairIS;
    private JLabel lblAverageWaitForHandoverIS;

    private StringBuilder mStringBuilder;
    private Simulation mSimulation;

    private DefaultCategoryDataset mSettlingDataSet;
    private DefaultCategoryDataset mVendorsDataSet;
    private DefaultCategoryDataset mMechanicsDataSet;

    private ExcelWriter mExcelWriter;

    private int mLoopIteration = 0;
    private int mLoopVendorActual = vendor_min;
    private int mLoopMechanicActual = mechanic_min;
    private boolean mLoopSimulation = false;

    private SimulationVariant mSimulationVariant = SimulationVariant.BOTH;

    private long mGraphWarmup;
    private long mGraphPeriod;
    private long mSimulationWarmup;
    private boolean mShowOnlyGlobalResults;

    public static void main(String[] args) {
        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(new MainForm().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setTitle("FRI ZA, Diskretna simulacia - Udalostna simulacia");
    }

    public MainForm() {
        init();
    }

    private void init() {
        mStringBuilder = new StringBuilder();
        SLOWRadioButton.setSelected(true);

        lblSimStatus.setText("Simulation initialized");

        btnStartSim.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startSimulation();
            }
        });

        btnStopSim.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stopSimulation();
            }
        });

        btnPauseOrResumeSim.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mSimulation.pauseSimulation();
            }
        });

        btnTestingScenarios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!mLoopSimulation) {
                    loopSimulation();
                }
            }
        });

        mShowOnlyGlobalResults = true;
        slider1.setMaximum(SLIDER_MAX_VALUE * 500);
        slider1.setValue(SLIDER_MAX_VALUE * 500);
        SlowDownConstraint.setFrequency(getSliderValue());
        slider1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider slider = (JSlider) e.getSource();
                SlowDownConstraint.setFrequency(getSliderValue(slider.getValue()));
            }
        });

        SLOWRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (SLOWRadioButton.isSelected()) {
                    SlowDownConstraint.setFrequency(getSliderValue());
                    mSimulation.changeMode(Speed.SLOW);
                } else {
                    mSimulation.changeMode(Speed.FAST);
                }
            }
        });

        radioBoth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioVendor.setSelected(false);
                radioMechanic.setSelected(false);
            }
        });
        radioVendor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioBoth.setSelected(false);
                radioMechanic.setSelected(false);
            }
        });
        radioMechanic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioVendor.setSelected(false);
                radioBoth.setSelected(false);
            }
        });
    }

    private double getSliderValue() {
        mShowOnlyGlobalResults = slider1.getValue() == SLIDER_MAX_VALUE * 500;
        SlowDownConstraint.setIsMax(slider1.getValue() == SLIDER_MAX_VALUE * 500);
        return (double) slider1.getValue() / SLIDER_MAX_VALUE;
    }

    private double getSliderValue(final long value) {
        mShowOnlyGlobalResults = slider1.getValue() == SLIDER_MAX_VALUE * 500;
        SlowDownConstraint.setIsMax(value == SLIDER_MAX_VALUE * 500);
        return (double) value / SLIDER_MAX_VALUE;
    }

    private void initSimulation(final long countOfReplication, final int countOfVendors, final int countOfMechanics) {
        textArea1.setLineWrap(true);
        textArea1.setWrapStyleWord(true);

        mSimulation = new Simulation(this, mGraphPeriod, countOfReplication,
                MAX_REPLICATION_TIME, countOfVendors, countOfMechanics);
    }

    private void startSimulation() {
        mGraphWarmup = Long.parseLong(txtGraphWarmup.getText());
        mGraphPeriod = Long.parseLong(txtGraphPeriod.getText());
        mSimulationWarmup = Long.parseLong(txtSimulationWarmup.getText());

        initSimulation(Long.parseLong(txtCountOfReplications.getText())
                , Integer.parseInt(txtCountOfVendors.getText()), Integer.parseInt(txtCountOfMechanics.getText()));
        mSettlingDataSet = new DefaultCategoryDataset();

        mSimulation.start();

    }

    private void stopSimulation() {
        if (mSimulation != null) {
            mSimulation.stopSimulation();
        }
    }

    @Override
    public void redrawLabels(Type simulationType, final double actualReplicationTime) {
        if (!mLoopSimulation && SimulationResult.mCountOfReplication >= mSimulationWarmup) {
            switch (simulationType) {
                case GLOBAL:
                    lblCountOfReplications.setText(SimulationResult.mCountOfReplication + "");

                    // average simulation time for one car repair
                    lblAverageTimeOfCarInSystem.setText(toHours(SimulationResult.getAverageCustomerTimeInService()) + " hours");
                    lblAverageTimeOfCarInService.setText(toHours(SimulationResult.getAverageCustomerWaitingForCar()) + " hours");
                    lblAverageTimeOfRepair.setText(toHours(SimulationResult.getAverageLengthOfRepair()) + " hours");

                    // average queue's length
                    lblAverageQueueLengthAcceptance.setText(getDecimalFormat(SimulationResult.getAverageLengthOfAcceptanceQueue()));
                    lblAverageQueueLengthHandover.setText(getDecimalFormat(SimulationResult.getAverageLengthOfForHandoverQueue()));
                    lblAverageQueueLengthForRepair.setText(getDecimalFormat(SimulationResult.getAverageLengthOfForRepairQueue()));

                    lblAverageCountOfFreeVendors.setText(getDecimalFormat(SimulationResult.getAverageLengthOfFreeVendorQueue()));
                    lblAverageCountOfFreeMechanics.setText(getDecimalFormat(SimulationResult.getAverageLengthOfFreeMechanicsQueue()));

                    // average waiting time in minutes
                    lblAverageWaitAcceptance.setText(toMinutes(SimulationResult.getAverageAcceptanceWaitTime()) + " minutes");
                    lblAverageWaitForRepair.setText(toMinutes(SimulationResult.getAverageForRepairWaitTime()) + " minutes");
                    lblAverageWaitForHandover.setText(toMinutes(SimulationResult.getAverageForHandoverWaitTime()) + " minutes");

                    lblAverageFiredCustomers.setText(getDecimalFormat(SimulationResult.getAverageCountOfFiredCustomers()));
                    lblAverageCoolingLength.setText(toHours(SimulationResult.getAverageCoolingUpLength()) + " hours");

                    lblAverageWaitAcceptanceIS.setText("<" + toMinutes(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageAcceptanceWaitTime, SimulationResult.mAverageAcceptanceWaitTimeSquare, SimulationResult.mCountOfReplication))
                            + "," + toMinutes(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageAcceptanceWaitTime, SimulationResult.mAverageAcceptanceWaitTimeSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageTimeOfCarInServiceIS.setText("<" + toHours(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageCustomerWaitingForCar, SimulationResult.mAverageCustomerWaitingForCarSquare, SimulationResult.mCountOfReplication))
                            + "," + toHours(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageCustomerWaitingForCar, SimulationResult.mAverageCustomerWaitingForCarSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageTimeOfCarInSystemIS.setText("<" + toHours(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageCustomerTimeInService, SimulationResult.mAverageCustomerTimeInServiceSquare, SimulationResult.mCountOfReplication))
                            + "," + toHours(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageCustomerTimeInService, SimulationResult.mAverageCustomerTimeInServiceSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageFiredCustomersIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageCountOfFiredCustomers, SimulationResult.mAverageCountOfFiredCustomersSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageCountOfFiredCustomers, SimulationResult.mAverageCountOfFiredCustomersSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageCoolingLengthIS.setText("<" + toHours(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageCoolingUpLength, SimulationResult.mAverageCoolingUpLengthSquare, SimulationResult.mCountOfReplication))
                            + "," + toHours(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageCoolingUpLength, SimulationResult.mAverageCoolingUpLengthSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageTimeOfRepairIS.setText("<" + toHours(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfRepair, SimulationResult.mAverageLengthOfRepairSquare, SimulationResult.mCountOfReplication))
                            + "," + toHours(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfRepair, SimulationResult.mAverageLengthOfRepairSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageCountOfFreeVendorsIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfFreeVendorQueue, SimulationResult.mAverageLengthOfFreeVendorQueueSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfFreeVendorQueue, SimulationResult.mAverageLengthOfFreeVendorQueueSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageCountOfFreeMechanicsIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfFreeMechanicsQueue, SimulationResult.mAverageLengthOfFreeMechanicsQueueSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfFreeMechanicsQueue, SimulationResult.mAverageLengthOfFreeMechanicsQueueSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageQueueLengthAcceptanceIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfAcceptanceQueue, SimulationResult.mAverageLengthOfAcceptanceQueueSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfAcceptanceQueue, SimulationResult.mAverageLengthOfAcceptanceQueueSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageQueueLengthForRepairIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfForRepairQueue, SimulationResult.mAverageLengthOfForRepairQueueSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfForRepairQueue, SimulationResult.mAverageLengthOfForRepairQueueSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageQueueLengthHandoverIS.setText("<" + (ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageLengthOfForHandoverQueue, SimulationResult.mAverageLengthOfForHandoverQueueSquare, SimulationResult.mCountOfReplication))
                            + "," + (ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageLengthOfForHandoverQueue, SimulationResult.mAverageLengthOfForHandoverQueueSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageWaitForHandoverIS.setText("<" + toMinutes(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageForHandoverWaitTime, SimulationResult.mAverageForHandoverWaitTimeSquare, SimulationResult.mCountOfReplication))
                            + "," + toMinutes(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageForHandoverWaitTime, SimulationResult.mAverageForHandoverWaitTimeSquare, SimulationResult.mCountOfReplication)) + ">");
                    lblAverageWaitForRepairIS.setText("<" + toMinutes(ConfidenceInterval.getMinConfidenceInterval(SimulationResult.mAverageForRepairWaitTime, SimulationResult.mAverageForRepairWaitTimeSquare, SimulationResult.mCountOfReplication))
                            + "," + toMinutes(ConfidenceInterval.getMaxConfidenceInterval(SimulationResult.mAverageForRepairWaitTime, SimulationResult.mAverageForRepairWaitTimeSquare, SimulationResult.mCountOfReplication)) + ">");
                    break;
                case REPLICATION:
                    if (!mShowOnlyGlobalResults) {
                        // actual replication time
                        lblReplTime.setText(getRealTime(ReplicationResult.mReplicationTime));

                        // queries length
                        lblCurrentAcceptanceQueue.setText(ReplicationResult.getAcceptanceQueueSize() + "");
                        lblCurrentForRepairQueue.setText(ReplicationResult.getForRepairQueueSize() + "");
                        lblCurrentForHandoverQueue.setText(ReplicationResult.getForHandoverQueueSize() + "");

                        // free employee arrays lengths
                        lblCountOfFreeMechanics.setText(ReplicationResult.mFreeMechanics.size() + "");
                        lblCountOfFreeVendors.setText(ReplicationResult.mFreeVendors.size() + "");

                        final ArrayList<Car> cars = ReplicationResult.getAllCarsFromSystem();
                        mStringBuilder.setLength(0);

                        mStringBuilder.append("Type\tRepair %\tEmployee\tLocation\n\n");
                        for (Car car : cars) {
                            final Employee employee = car.getEmployee();
                            if (null != employee) {
                                mStringBuilder.append(car.getCarID() + "\t" + car.getPercentageOfRepair(actualReplicationTime)
                                        + "\t" + car.getEmployee().getEmployeeType().toString() + "\t" + car.getCarLocation().toString() + "\n");
                            } else {
                                mStringBuilder.append(car.getCarID() + "\t" + car.getPercentageOfRepair(actualReplicationTime)
                                        + "\t" + null + "\t" + car.getCarLocation().toString() + "\n");
                            }
                        }

                        textArea1.setText("");
                        textArea1.setText(mStringBuilder.toString());
                    }
                    break;
            }
        }
    }

    private String getRealTime(final double timeInSeconds) {
        final int hours = (int) Math.floor(timeInSeconds / 3600) % 8;
        int minutes = (int) Math.floor((timeInSeconds % 3600) / 60);
        int seconds = (int) Math.floor(timeInSeconds % 60);

        final int day = (int) Math.floor((timeInSeconds / 3600) / 8);

        if (minutes < 10 & seconds < 10) {
            return day + " day, " + (hours + 7) + ":0" + minutes + ":0" + seconds;
        } else if (minutes < 10) {
            return day + " day, " + (hours + 7) + ":0" + minutes + ":" + seconds;
        } else if (seconds < 10) {
            return day + " day, " + (hours + 7) + ":" + minutes + ":0" + seconds;
        } else {
            return day + " day, " + (hours + 7) + ":" + minutes + ":" + seconds;
        }
    }

    private String toHours(final double timeInSeconds) {
        return getDecimalFormat(timeInSeconds / 60 / 60);
    }

    private double toHour(final double timeInSeconds) {
        return ConfidenceInterval.round(timeInSeconds / 60 / 60, 6);
    }

    private String toMinutes(final double timeInSeconds) {
        return getDecimalFormat(timeInSeconds / 60);
    }

    private static String getDecimalFormat(final double value) {
        DecimalFormat df = new DecimalFormat("#.######");
        return df.format(value).toString();
    }

    @Override
    public void redrawGraphs() {
        if (!mLoopSimulation && SimulationResult.mCountOfReplication > mGraphWarmup) {
            mSettlingDataSet.addValue(toHour(SimulationResult.getAverageCustomerWaitingForCar()), "Length of customer waiting", SimulationResult.getCountOfReplication() + "");
            JFreeChart chart = ChartFactory.createLineChart("Length of customer waiting for a car", "Replication", "Customer wait for repair length in hours", mSettlingDataSet);

            final CategoryPlot plot = chart.getCategoryPlot();
            plot.getRangeAxis().setAutoRange(true);

            final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
            rangeAxis.setAutoRangeIncludesZero(false);

            panelSettlingChart.setLayout(new FlowLayout());

            ChartPanel panel = new ChartPanel(chart);
            panelSettlingChart.removeAll();
            panelSettlingChart.add(panel);
            panelSettlingChart.updateUI();
        }
    }

    @Override
    public void simulationStarted() {
        if (!mLoopSimulation) {
            lblSimStatus.setText("Simulation started");
        }
    }

    @Override
    public void simulationFinished() {
        if (mLoopSimulation) {
            switch (mSimulationVariant) {
                case BOTH:
                    mExcelWriter.writeLineToExcel(mLoopIteration, mLoopVendorActual, mLoopMechanicActual);

                    if (110 <= mLoopIteration) {
                        mLoopSimulation = false;
                        lblSimStatus.setText("Simulation done " + mLoopIteration + "/110");
                    } else {
                        mLoopMechanicActual++;
                        if (mechanic_max < mLoopMechanicActual) {
                            mLoopVendorActual++;
                            mLoopMechanicActual = mechanic_min;
                        }

                        startAnotherLoop(mLoopVendorActual, mLoopMechanicActual);
                    }
                    break;
                case VENDORS:
                    redrawVariableVendorsGraph();

                    if (10 <= mLoopIteration) {
                        mLoopSimulation = false;
                        lblSimStatus.setText("Simulation done " + mLoopIteration + "/10");
                    } else {
                        startAnotherLoop(mLoopVendorActual++, mLoopMechanicActual);
                    }
                    break;
                case MECHANICS:
                    redrawVariableMechanicsGraph();

                    if (10 <= mLoopIteration) {
                        mLoopSimulation = false;
                        lblSimStatus.setText("Simulation done " + mLoopIteration + "/10");
                    } else {
                        startAnotherLoop(mLoopVendorActual, mLoopMechanicActual++);
                    }
                    break;
            }
        } else {
            redrawLabels(Type.GLOBAL, MAX_REPLICATION_TIME);
            lblSimStatus.setText("Simulation finished");
        }
    }

    @Override
    public void simulationPausedOrResume(boolean isPaused, final double actualReplicationTime) {
        if (isPaused) {
            btnPauseOrResumeSim.setText("Resume");
            redrawLabels(Type.GLOBAL, actualReplicationTime);
            redrawLabels(Type.REPLICATION, actualReplicationTime);
            lblSimStatus.setText("Simulation paused");
        } else {
            btnPauseOrResumeSim.setText("Pause");
            lblSimStatus.setText("Simulation resumed");

        }
    }

    private void loopSimulation() {
        initVariant();

        mLoopIteration = 0;
        switch (mSimulationVariant) {
            case BOTH:
                mLoopVendorActual = vendor_min;
                mLoopMechanicActual = mechanic_min;
                break;
            case VENDORS:
                mLoopVendorActual = vendor_min;
                break;
            case MECHANICS:
                mLoopMechanicActual = mechanic_min;
                break;
        }

        mVendorsDataSet = new DefaultCategoryDataset();
        mMechanicsDataSet = new DefaultCategoryDataset();

        mLoopSimulation = true;
        mExcelWriter = new ExcelWriter("sim_results.xlsx");

        switch (mSimulationVariant) {
            case BOTH:
                break;
            case VENDORS:
                mLoopMechanicActual = Integer.parseInt(txtCountOfMechanics.getText());
                break;
            case MECHANICS:
                mLoopVendorActual = Integer.parseInt(txtCountOfVendors.getText());
                break;
        }

        startAnotherLoop(mLoopVendorActual, mLoopMechanicActual);
    }

    private void initVariant() {
        if (radioBoth.isSelected()) {
            mSimulationVariant = SimulationVariant.BOTH;
        } else if (radioMechanic.isSelected()) {
            mSimulationVariant = SimulationVariant.MECHANICS;
        } else if (radioVendor.isSelected()) {
            mSimulationVariant = SimulationVariant.VENDORS;
        }
    }

    private void startAnotherLoop(final int vendorCount, final int mechanicCount) {
        mLoopIteration++;

        switch (mSimulationVariant) {
            case BOTH:
                lblSimStatus.setText("Simulation working " + mLoopIteration + "/110");
                break;
            case VENDORS:
            case MECHANICS:
                lblSimStatus.setText("Simulation working " + mLoopIteration + "/10");
                break;
        }

        mSimulation = new Simulation(this, 100000, Long.parseLong(txtCountOfReplications.getText()), MAX_REPLICATION_TIME, vendorCount, mechanicCount);
        mSimulation.start();
    }

    public void redrawVariableVendorsGraph() {
        mVendorsDataSet.addValue(toHour(SimulationResult.getAverageCustomerWaitingForCar()), "Length of customer waiting", mLoopVendorActual + "");
        JFreeChart chart = ChartFactory.createLineChart("Length of customer waiting for a car", "Count of vendors", "Customer wait for repair length in hours", mVendorsDataSet);

        final CategoryPlot plot = chart.getCategoryPlot();
        plot.getRangeAxis().setAutoRange(true);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);

        panelVendorChart.setLayout(new FlowLayout());

        ChartPanel panel = new ChartPanel(chart);
        panelVendorChart.removeAll();
        panelVendorChart.add(panel);
        panelVendorChart.updateUI();
    }

    public void redrawVariableMechanicsGraph() {
        mMechanicsDataSet.addValue(toHour(SimulationResult.getAverageCustomerWaitingForCar()), "Length of customer waiting", mLoopMechanicActual + "");
        JFreeChart chart = ChartFactory.createLineChart("Length of customer waiting for a car", "Count of mechanics", "Customer wait for repair length in hours", mMechanicsDataSet);

        final CategoryPlot plot = chart.getCategoryPlot();
        plot.getRangeAxis().setAutoRange(true);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);

        panelMechanicChart.setLayout(new FlowLayout());

        ChartPanel panel = new ChartPanel(chart);
        panelMechanicChart.removeAll();
        panelMechanicChart.add(panel);
        panelMechanicChart.updateUI();
    }

    private enum SimulationVariant {
        BOTH, VENDORS, MECHANICS
    }
}
